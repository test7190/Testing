/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;
import da.StudentDA;
import domain.Student;
/**
 *
 * @author harch
 */
public class MaintainStudentControl {
    private StudentDA stdDA;
    
    public MaintainStudentControl() {
        stdDA = new StudentDA();
    }
    
    public Student selectRecord(String id) {
        return stdDA.getRecord(id);
    }
    
    public void addRecord(Student student) {
        stdDA.addRecord(student);
    }
    
    public void updateRecord(Student student) {
        stdDA.updateRecord(student);
    }
}
